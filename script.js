const scrollToTopButton = document.querySelector("#arrow-box");

function scrollToTop() {
  document.documentElement.scrollTop = 0;
}

scrollToTopButton.onclick = function () {
  scrollToTop();
};
